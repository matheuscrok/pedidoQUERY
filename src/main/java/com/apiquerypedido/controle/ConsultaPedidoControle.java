package com.apiquerypedido.controle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.apiquerypedido.entidade.dto.Requisicao.ReqPedidoDTO;
import com.apiquerypedido.entidade.dto.Resposta.ResPedidoDTO;
import com.apiquerypedido.negocio.NegocioConsultaPedido;
import com.apiquerypedido.utilidades.ResponseConvert;
import com.apiquerypedido.utilidades.excecoes.DataCadastroNecessariaException;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/pedidos")
@CrossOrigin(origins = "*")
public class ConsultaPedidoControle {
    private static final Logger log = LoggerFactory.getLogger(ConsultaPedidoControle.class);

    private NegocioConsultaPedido negocioConsultaPedido;

    public ConsultaPedidoControle(NegocioConsultaPedido negocioConsultaPedido) {
        this.negocioConsultaPedido = negocioConsultaPedido;
    }

    @GetMapping
    public ResponseEntity<List<ResPedidoDTO>> buscarTodos() {
        List<ResPedidoDTO> pedidos = negocioConsultaPedido.listarTodos();
        return ResponseEntity.ok(pedidos);
    }

    @GetMapping("/{numeroControle}")
    public ResponseEntity<ResPedidoDTO> buscarPorNumeroControle(@PathVariable Long numeroControle) {
        ResPedidoDTO pedido = negocioConsultaPedido.buscarPorNumeroControle(numeroControle);
        return pedido != null ? ResponseEntity.ok(pedido) : ResponseEntity.notFound().build();
    }

    @GetMapping("/dataCadastro/{dataCadastro}")
    public ResponseEntity<List<ResPedidoDTO>> buscarPorDataCadastro(
            @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataCadastro) {
        List<ResPedidoDTO> pedidos = negocioConsultaPedido.buscarPorDataCadastro(dataCadastro);
        return !pedidos.isEmpty() ? ResponseEntity.ok(pedidos) : ResponseEntity.notFound().build();
    }

    @GetMapping("/filtro")
    public ResponseEntity<ResponseConvert> filtrarPedidos(ReqPedidoDTO reqPedidoDTO) {
        try {
            List<ResPedidoDTO> pedidosFiltrados = negocioConsultaPedido.filtrarPedidos(reqPedidoDTO);
            return ResponseEntity.ok(ResponseConvert.ofPedidos(pedidosFiltrados));
        } catch (DataCadastroNecessariaException e) {
            return ResponseEntity.badRequest()
                    .body(ResponseConvert.ofErrors(Collections.singletonList(e.getMessage())));
        } catch (IllegalArgumentException e) {
            log.error("Erro de validação: {}", e.getMessage());
            return ResponseEntity.badRequest()
                    .body(ResponseConvert.ofErrors(Collections.singletonList(e.getMessage())));
        } catch (RuntimeException e) {
            log.error("Erro interno no servidor: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ResponseConvert.ofErrors(Collections.singletonList("Erro interno no servidor")));
        } catch (Exception e) {
            log.error("Erro interno no servidor: {}", e.getMessage());
            return ResponseEntity.badRequest()
                    .body(ResponseConvert.ofErrors(Collections.singletonList(e.getMessage())));
        }
    }

}