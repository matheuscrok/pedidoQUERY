package com.apiquerypedido.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apiquerypedido.entidade.Pedido;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface RepositorioPedido extends JpaRepository<Pedido, Long> {

    List<Pedido> findByDataCadastro(LocalDate dataCadastro);

    Optional<Pedido> findByNumeroControle(Long numeroControle);

    Collection<Pedido> findByNumeroControleAndDataCadastro(Long numeroControle, LocalDate dataCadastro);
}
