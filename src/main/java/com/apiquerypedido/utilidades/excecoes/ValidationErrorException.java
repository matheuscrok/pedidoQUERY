package com.apiquerypedido.utilidades.excecoes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ValidationErrorException extends RuntimeException {

     private static final Logger log = LoggerFactory.getLogger(ValidationErrorException.class);
    public ValidationErrorException(String message) {
        super(message);
        log.error("Erro de validação: {}", message);
    }

    public ValidationErrorException(String message, Throwable cause) {
        super(message, cause);
        log.error("Erro interno no servidor: {}", cause.getMessage());
    }

}
