package com.apiquerypedido.utilidades.excecoes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataCadastroNecessariaException extends RuntimeException {
    private static final Logger log = LoggerFactory.getLogger(DataCadastroNecessariaException.class);
    public DataCadastroNecessariaException(String message) {
        super(message);
        log.error("Erro de validação: {}", message);
    }
}
