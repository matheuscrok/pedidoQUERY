package com.apiquerypedido.utilidades.excecoes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PedidoFilterException extends RuntimeException {
    private static final Logger log = LoggerFactory.getLogger(PedidoFilterException.class);
    public PedidoFilterException(String message, Throwable cause) {
        super(message, cause);
        log.error("Erro interno no servidor: {}", cause.getMessage());
    }

    public PedidoFilterException(String message) {
        super(message);
        log.error("Erro de validação: {}", message);
    }
}
