package com.apiquerypedido.utilidades;

import java.util.List;

import com.apiquerypedido.entidade.dto.Resposta.ResPedidoDTO;

public class ResponseConvert {
    private final List<ResPedidoDTO> pedidos;
    private final List<String> errors;

    private ResponseConvert(List<ResPedidoDTO> pedidos, List<String> errors) {
        this.pedidos = pedidos;
        this.errors = errors;
    }

    public static ResponseConvert ofPedidos(List<ResPedidoDTO> pedidos) {
        return new ResponseConvert(pedidos, null);
    }

    public static ResponseConvert ofErrors(List<String> errors) {
        return new ResponseConvert(null, errors);
    }

    public List<ResPedidoDTO> getPedidos() {
        return pedidos;
    }

    public List<String> getErrors() {
        return errors;
    }

    public boolean hasErrors() {
        return errors != null && !errors.isEmpty();
    }
}
