package com.apiquerypedido.utilidades;

import java.util.List;
import java.util.stream.Collectors;

import com.apiquerypedido.entidade.DetalhePedido;
import com.apiquerypedido.entidade.Pedido;
import com.apiquerypedido.entidade.dto.Resposta.ResDetalhePedidoDTO;
import com.apiquerypedido.entidade.dto.Resposta.ResPedidoDTO;

public class DTOConverter {

    public static ResPedidoDTO convertPedidoEntityToDTO(Pedido pedido) {
        List<ResDetalhePedidoDTO> detalhesDTO = pedido.getDetalhes().stream()
                .map(DTOConverter::convertDetalhePedidoEntityToDTO)
                .collect(Collectors.toList());
        return new ResPedidoDTO(pedido.getNumeroControle(), pedido.getDataCadastro(), pedido.getValorTotal(),
                pedido.getCodigoCliente(), detalhesDTO);
    }

    public static ResDetalhePedidoDTO convertDetalhePedidoEntityToDTO(DetalhePedido detalhePedido) {
        return new ResDetalhePedidoDTO(detalhePedido.getId(), detalhePedido.getQuantidade(),
                detalhePedido.getPrecoUnitario(), detalhePedido.getNomeProduto());
    }
}
