package com.apiquerypedido.negocio;

import com.apiquerypedido.entidade.DetalhePedido;
import com.apiquerypedido.entidade.Pedido;
import com.apiquerypedido.entidade.dto.Requisicao.ReqPedidoDTO;
import com.apiquerypedido.entidade.dto.Resposta.ResPedidoDTO;
import com.apiquerypedido.repositorio.RepositorioPedido;
import com.apiquerypedido.utilidades.DTOConverter;
import com.apiquerypedido.utilidades.excecoes.ValidationErrorException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.Data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Data
public class NegocioConsultaPedido {

    private static final Logger log = LoggerFactory.getLogger(NegocioConsultaPedido.class);

    private RepositorioPedido pedidoRepository;

    public NegocioConsultaPedido(RepositorioPedido pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }
    public List<ResPedidoDTO> listarTodos() {
        return pedidoRepository.findAll().stream()
                .map(DTOConverter::convertPedidoEntityToDTO)
                .collect(Collectors.toList());
    }

    public ResPedidoDTO buscarPorNumeroControle(Long numeroControle) {
        return pedidoRepository.findByNumeroControle(numeroControle)
                .map(DTOConverter::convertPedidoEntityToDTO)
                .orElse(null);
    }

    public List<ResPedidoDTO> buscarPorDataCadastro(LocalDate dataCadastro) throws IllegalArgumentException {
        return pedidoRepository.findByDataCadastro(dataCadastro).stream()
                .map(DTOConverter::convertPedidoEntityToDTO)
                .collect(Collectors.toList());
    }

    public List<ResPedidoDTO> filtrarPedidos(ReqPedidoDTO reqPedidoDTO) {
        if (reqPedidoDTO.numeroControle() != null && reqPedidoDTO.dataCadastro() == null) {
            log.error("A data de cadastro deve ser informada quando o número de controle é especificado.");
            throw new IllegalArgumentException("A data de cadastro deve ser informada quando o número de controle é especificado.");
        }

        try {
            return aplicarFiltros(reqPedidoDTO);
        } catch (Exception e) {
            throw new ValidationErrorException("Erro interno no servidor", e);
        }
    }

    private List<ResPedidoDTO> aplicarFiltros(ReqPedidoDTO reqPedidoDTO) {
        if (reqPedidoDTO.numeroControle() != null && reqPedidoDTO.dataCadastro() != null) {
            return pedidoRepository.findByNumeroControleAndDataCadastro(reqPedidoDTO.numeroControle(), reqPedidoDTO.dataCadastro()).stream()
                    .map(DTOConverter::convertPedidoEntityToDTO)
                    .collect(Collectors.toList());
        } else if (reqPedidoDTO.numeroControle() != null) {
            return pedidoRepository.findByNumeroControle(reqPedidoDTO.numeroControle()).stream()
                    .map(DTOConverter::convertPedidoEntityToDTO)
                    .collect(Collectors.toList());
        } else if (reqPedidoDTO.dataCadastro() != null) {
            return pedidoRepository.findByDataCadastro(reqPedidoDTO.dataCadastro()).stream()
                    .map(DTOConverter::convertPedidoEntityToDTO)
                    .collect(Collectors.toList());
        } else {
            return listarTodos();
        }
    }

    @KafkaListener(topics = "pedido-events", groupId = "#{T(java.lang.System).getenv('SPRING_KAFKA_CONSUMER_GROUP_ID')}")
    public void listenPedidoEvent(String message) {
        Pedido jsonMessage = converterDeJson(message);
        if (Objects.nonNull(jsonMessage)) {
            pedidoRepository.save(jsonMessage);
        }
    }

    public Pedido converterDeJson(String message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            ResPedidoDTO resPedidoDTO = mapper.readValue(message, ResPedidoDTO.class);
            List<DetalhePedido> detalhes = resPedidoDTO.detalhes().stream()
                    .map(d -> new DetalhePedido(d.id(), d.quantidade() != null ? d.quantidade() : 1, d.precoUnitario(), d.nomeProduto(), resPedidoDTO.numeroControle()))
                    .collect(Collectors.toList());
            return new Pedido(resPedidoDTO.numeroControle(), resPedidoDTO.dataCadastro(), resPedidoDTO.codigoCliente(), resPedidoDTO.valorTotal(), detalhes);
        } catch (JsonProcessingException e) {
            throw new ValidationErrorException("Erro ao converter json para objeto", e);
        }
    }
}
