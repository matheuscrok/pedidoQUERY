package com.apiquerypedido.entidade.dto.Resposta;

import java.math.BigDecimal;

public record ResDetalhePedidoDTO(Long id, Integer quantidade, BigDecimal precoUnitario, String nomeProduto) {
}