package com.apiquerypedido.entidade.dto.Requisicao;

import java.math.BigDecimal;

public record ReqDetalhePedidoDTO(Long id, Integer quantidade, BigDecimal precoUnitario, String nomeProduto) {
}