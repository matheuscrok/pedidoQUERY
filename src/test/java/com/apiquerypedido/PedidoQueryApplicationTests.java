package com.apiquerypedido;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertTrue;
@ActiveProfiles("test")

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class PedidoQueryApplicationTests {
    @Test
    void contextLoads() {
        assertTrue(true, "Contexto carregado com sucesso");
    }
}
