package com.apiquerypedido;

import com.apiquerypedido.entidade.Pedido;
import com.apiquerypedido.entidade.dto.Requisicao.ReqPedidoDTO;
import com.apiquerypedido.entidade.dto.Resposta.ResPedidoDTO;
import com.apiquerypedido.negocio.NegocioConsultaPedido;
import com.apiquerypedido.repositorio.RepositorioPedido;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
@ActiveProfiles("test")

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class NegocioConsultaPedidoTests {

    @MockBean
    private RepositorioPedido repositorioPedido;
    @InjectMocks
    private NegocioConsultaPedido negocioConsultaPedido;
    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void testBuscarPedidosPorDataCadastro() {
        LocalDate data = LocalDate.now();
        List<Pedido> pedidosMock = List.of(
                new Pedido(1L, data, 1L, BigDecimal.valueOf(1000), Collections.emptyList()),
                new Pedido(2L, data, 2L, BigDecimal.valueOf(2000), Collections.emptyList())
        );
        when(repositorioPedido.findByDataCadastro(data)).thenReturn(pedidosMock);

        List<ResPedidoDTO> resultado = negocioConsultaPedido.buscarPorDataCadastro(data);

        assertFalse(resultado.isEmpty(), "The result should not be empty");
        assertEquals(pedidosMock.size(), resultado.size(), "The number of orders should match the mock data");
        for (int i = 0; i < pedidosMock.size(); i++) {
            Pedido pedidoMock = pedidosMock.get(i);
            ResPedidoDTO resPedidoDTO = resultado.get(i);
            assertEquals(pedidoMock.getNumeroControle(), resPedidoDTO.numeroControle(), "Order control number should match");
            assertEquals(pedidoMock.getDataCadastro(), resPedidoDTO.dataCadastro(), "Order registration date should match");
            assertEquals(pedidoMock.getValorTotal(), resPedidoDTO.valorTotal(), "Order total value should match");
        }
    }


    @Test
    void testBuscarPedidosPorDataCadastroSemPedidos() {
        LocalDate data = LocalDate.now();
        when(repositorioPedido.findByDataCadastro(data)).thenReturn(Collections.emptyList());

        List<ResPedidoDTO> resultado = negocioConsultaPedido.buscarPorDataCadastro(data);

        assertTrue(resultado.isEmpty());
    }

    @Test
    void testListarTodos() {
        List<Pedido> pedidosMock = List.of(new Pedido(1L, LocalDate.now(), 1L, BigDecimal.valueOf(1000), Collections.emptyList()));
        when(repositorioPedido.findAll()).thenReturn(pedidosMock);

        List<ResPedidoDTO> resultado = negocioConsultaPedido.listarTodos();

        assertFalse(resultado.isEmpty());
        assertEquals(1, resultado.size());
    }


    @Test
    void testBuscarPorNumeroControle() {
        Long numeroControle = 1L;
        Pedido pedidoMock = new Pedido(numeroControle, LocalDate.now(), 1L, BigDecimal.valueOf(1000), Collections.emptyList());
        when(repositorioPedido.findByNumeroControle(numeroControle)).thenReturn(Optional.of(pedidoMock));

        ResPedidoDTO resultado = negocioConsultaPedido.buscarPorNumeroControle(numeroControle);

        assertNotNull(resultado);
        assertEquals(numeroControle, resultado.numeroControle());
    }


    @Test
    void testFiltrarPedidosPorNumeroControleEData() {
        Long numeroControle = 1L;
        LocalDate data = LocalDate.now();
        ReqPedidoDTO reqPedidoDTO = new ReqPedidoDTO(numeroControle, data, null, null, null);
        List<Pedido> pedidosMock = List.of(new Pedido(numeroControle, data, 1L, BigDecimal.valueOf(1000), List.of()));
        when(repositorioPedido.findByNumeroControleAndDataCadastro(numeroControle, data))
                .thenReturn(pedidosMock);

        List<ResPedidoDTO> resultado = negocioConsultaPedido.filtrarPedidos(reqPedidoDTO);

        assertFalse(resultado.isEmpty());
        assertEquals(pedidosMock.size(), resultado.size());
        assertEquals(numeroControle, resultado.get(0).numeroControle());
        assertEquals(data, resultado.get(0).dataCadastro());
    }


    @Test
    void testConversaoJsonParaPedido() {
        String jsonPedido = "{\"numeroControle\":1,\"dataCadastro\":\"2021-01-01\",\"valorTotal\":1000,\"codigoCliente\":1,\"detalhes\":[{\"quantidade\":1,\"precoUnitario\":100}]}";

        Pedido pedido = negocioConsultaPedido.converterDeJson(jsonPedido);

        assertNotNull(pedido);
    }


    @Test
    void testListarTodosPedidos() {
        List<Pedido> pedidosMock = List.of(new Pedido(1L, LocalDate.now(), 1L, BigDecimal.valueOf(1000), List.of()));
        when(repositorioPedido.findAll()).thenReturn(pedidosMock);

        List<ResPedidoDTO> resultado = negocioConsultaPedido.listarTodos();

        assertFalse(resultado.isEmpty());
        assertEquals(1, resultado.size());
    }

    @Test
    void testBuscarPedidoPorNumeroControle() {
        Long numeroControle = 1L;
        Pedido pedidoMock = new Pedido(numeroControle, LocalDate.now(), 1L, BigDecimal.valueOf(1000), List.of());
        when(repositorioPedido.findByNumeroControle(numeroControle)).thenReturn(Optional.of(pedidoMock));

        ResPedidoDTO resultado = negocioConsultaPedido.buscarPorNumeroControle(numeroControle);

        assertNotNull(resultado);
        assertEquals(numeroControle, resultado.numeroControle());
    }

    @Test
    void testFiltrarPedidosSemFiltros() {
        List<Pedido> pedidosMock = List.of(new Pedido(1L, LocalDate.now(), 1L, BigDecimal.valueOf(1000), List.of()));
        when(repositorioPedido.findAll()).thenReturn(pedidosMock);

        List<ResPedidoDTO> resultado = negocioConsultaPedido.listarTodos();

        assertFalse(resultado.isEmpty());
        assertEquals(1, resultado.size());
    }

    @Test
    void testTratamentoExcecoesNaConversaoJson() {
        String jsonInvalido = "{\"numeroControle\":\"abc\",\"dataCadastro\":\"2021-01-01\",\"valorTotal\":1000,\"codigoCliente\":1,\"detalhes\":[]}";
        assertThrows(RuntimeException.class, () -> negocioConsultaPedido.converterDeJson(jsonInvalido));
    }

    @Test
    void testConversaoEntidadeParaDto() {
        Pedido pedidoMock = new Pedido(1L, LocalDate.now(), 1L, BigDecimal.valueOf(1000), List.of());
        when(repositorioPedido.findAll()).thenReturn(List.of(pedidoMock));

        List<ResPedidoDTO> resultado = negocioConsultaPedido.listarTodos();

        assertFalse(resultado.isEmpty());
        ResPedidoDTO dto = resultado.get(0);
        assertNotNull(dto);
        assertEquals(pedidoMock.getNumeroControle(), dto.numeroControle());
    }

    @Test
    void testBuscarPedidoPorNumeroControleInexistente() {
        Long numeroControleInexistente = 999L;
        when(repositorioPedido.findByNumeroControle(numeroControleInexistente)).thenReturn(Optional.empty());

        ResPedidoDTO resultado = negocioConsultaPedido.buscarPorNumeroControle(numeroControleInexistente);

        assertNull(resultado);
    }

}
