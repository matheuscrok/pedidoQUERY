package com.apiquerypedido;

import com.apiquerypedido.controle.ConsultaPedidoControle;
import com.apiquerypedido.entidade.dto.Requisicao.ReqDetalhePedidoDTO;
import com.apiquerypedido.entidade.dto.Requisicao.ReqPedidoDTO;
import com.apiquerypedido.entidade.dto.Resposta.ResPedidoDTO;
import com.apiquerypedido.negocio.NegocioConsultaPedido;
import com.apiquerypedido.utilidades.ResponseConvert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class ConsultaPedidoControleTests {

    @MockBean
    private NegocioConsultaPedido negocioConsultaPedido;

    @InjectMocks
    private ConsultaPedidoControle consultaPedidoControle;
    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void testBuscarTodosPedidosVazios() {
        when(negocioConsultaPedido.listarTodos()).thenReturn(List.of());

        ResponseEntity<List<ResPedidoDTO>> resposta = consultaPedidoControle.buscarTodos();

        assertEquals(HttpStatus.OK, resposta.getStatusCode());

        assertTrue(Optional.ofNullable(resposta.getBody()).orElse(Collections.emptyList()).isEmpty(),
                "A lista de pedidos deve estar vazia.");
    }

    @Test
    void testBuscarTodosPedidosComDados() {
        List<ResPedidoDTO> pedidosMock = List
                .of(new ResPedidoDTO(1L, LocalDate.now(), BigDecimal.valueOf(1000), 1L, List.of()));
        when(negocioConsultaPedido.listarTodos()).thenReturn(pedidosMock);

        ResponseEntity<List<ResPedidoDTO>> resposta = consultaPedidoControle.buscarTodos();

        assertEquals(HttpStatus.OK, resposta.getStatusCode());

        List<ResPedidoDTO> responseBody = Optional.ofNullable(resposta.getBody()).orElse(Collections.emptyList());
        assertEquals(pedidosMock.size(), responseBody.size(), "A lista de pedidos deve conter um elemento.");
    }

    @Test
    void testBuscarPedidoPorNumeroControleValido() {
        Long numeroControle = 1L;
        ResPedidoDTO pedidoMock = new ResPedidoDTO(numeroControle, LocalDate.now(), BigDecimal.valueOf(1000), 1L,
                List.of());
        when(negocioConsultaPedido.buscarPorNumeroControle(numeroControle)).thenReturn(pedidoMock);

        ResponseEntity<ResPedidoDTO> resposta = consultaPedidoControle.buscarPorNumeroControle(numeroControle);

        assertEquals(HttpStatus.OK, resposta.getStatusCode());

        ResPedidoDTO responseBody = Optional.ofNullable(resposta.getBody()).orElseThrow(
                () -> new AssertionError("O corpo da resposta não deve ser nulo."));

        assertEquals(numeroControle, responseBody.numeroControle());
    }

    @Test
    void testBuscarPedidoPorNumeroControleInexistente() {
        Long numeroControle = 1L;
        when(negocioConsultaPedido.buscarPorNumeroControle(numeroControle)).thenReturn(null);

        ResponseEntity<ResPedidoDTO> resposta = consultaPedidoControle.buscarPorNumeroControle(numeroControle);

        assertEquals(HttpStatus.NOT_FOUND, resposta.getStatusCode());
    }

    @Test
    void testBuscarPorDataCadastroComPedidos() {
        LocalDate data = LocalDate.now();
        List<ResPedidoDTO> pedidosMock = List.of(new ResPedidoDTO(1L, data, BigDecimal.valueOf(1000), 1L, List.of()));
        when(negocioConsultaPedido.buscarPorDataCadastro(data)).thenReturn(pedidosMock);

        ResponseEntity<List<ResPedidoDTO>> resposta = consultaPedidoControle.buscarPorDataCadastro(data);

        assertEquals(HttpStatus.OK, resposta.getStatusCode());
        assertEquals(pedidosMock.size(),
                Optional.ofNullable(resposta.getBody()).orElse(Collections.emptyList()).size());
    }

    @Test
    void testBuscarPorDataCadastroSemPedidos() {
        LocalDate data = LocalDate.now();
        List<ResPedidoDTO> listaVazia = List.of();
        when(negocioConsultaPedido.buscarPorDataCadastro(data)).thenReturn(listaVazia);

        ResponseEntity<List<ResPedidoDTO>> resposta = consultaPedidoControle.buscarPorDataCadastro(data);

        assertEquals(HttpStatus.NOT_FOUND, resposta.getStatusCode());
    }

    @Test
    void testFiltrarPedidosPorNumeroControleEData() {
        Long numeroControle = 1L;
        LocalDate data = LocalDate.now();
        List<ResPedidoDTO> pedidosMock = List
                .of(new ResPedidoDTO(numeroControle, data, BigDecimal.valueOf(1000), 1L, List.of()));
        ReqPedidoDTO pedidoDTO = new ReqPedidoDTO(numeroControle, data, BigDecimal.valueOf(1000), 1L, List.of());
        when(negocioConsultaPedido.filtrarPedidos(pedidoDTO)).thenReturn(pedidosMock);

        ResponseEntity<ResponseConvert> resposta = consultaPedidoControle.filtrarPedidos(pedidoDTO);

        assertEquals(HttpStatus.OK, resposta.getStatusCode());

        // Use Optional to safely handle potential null
        ResponseConvert responseBody = Optional.ofNullable(resposta.getBody()).orElseThrow(
                () -> new AssertionError("A resposta não deve ser nula"));
        List<ResPedidoDTO> pedidos = Optional.ofNullable(responseBody.getPedidos()).orElseThrow(
                () -> new AssertionError("A lista de pedidos não deve ser nula"));

        assertEquals(pedidosMock.size(), pedidos.size());
    }

    @Test
    void testFiltrarPedidosSemFiltros() {
        when(negocioConsultaPedido.listarTodos()).thenReturn(List.of());

        ResponseEntity<List<ResPedidoDTO>> resposta = consultaPedidoControle.buscarTodos();

        assertEquals(HttpStatus.OK, resposta.getStatusCode());
    }

    @Test
    void testFiltrarPedidosComNumeroControleSemData() {
        Long numeroControle = 1L;
        BigDecimal valorTotal = new BigDecimal("100.00");
        Long codigoCliente = 1L;
        List<ReqDetalhePedidoDTO> detalhes = List
                .of(new ReqDetalhePedidoDTO(null, 1, new BigDecimal("100.00"), "Produto X"));
        ReqPedidoDTO reqPedidoDTO = new ReqPedidoDTO(numeroControle, null, valorTotal, codigoCliente, detalhes);

        when(negocioConsultaPedido.filtrarPedidos(reqPedidoDTO)).thenThrow(new IllegalArgumentException(
                "A data de cadastro deve ser informada quando o número de controle é especificado."));

        ResponseEntity<ResponseConvert> resposta = consultaPedidoControle.filtrarPedidos(reqPedidoDTO);

        assertEquals(HttpStatus.BAD_REQUEST, resposta.getStatusCode());
        String expectedErrorMessage = "A data de cadastro deve ser informada quando o número de controle é especificado.";

        Optional<ResponseConvert> optionalBody = Optional.ofNullable(resposta.getBody());
        boolean containsErrorMessage = optionalBody
                .map(ResponseConvert::getErrors)
                .filter(errors -> !errors.isEmpty())
                .map(List::getFirst)
                .filter(expectedErrorMessage::contains)
                .isPresent();

        assertTrue(containsErrorMessage, "A mensagem de erro esperada não foi encontrada.");

    }

    @Test
    void testTratamentoExcecoesInesperadas() {
        Long numeroControle = 1L;
        LocalDate data = LocalDate.now();
        ReqPedidoDTO pedidoDTO = new ReqPedidoDTO(numeroControle, data, BigDecimal.valueOf(1000), 1L, List.of());
        when(negocioConsultaPedido.filtrarPedidos(pedidoDTO))
                .thenThrow(new RuntimeException("Erro interno no servidor."));

        ResponseEntity<ResponseConvert> resposta = consultaPedidoControle.filtrarPedidos(pedidoDTO);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, resposta.getStatusCode());
    }

}
