package com.apiquerypedido;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.apiquerypedido.entidade.Pedido;
import com.apiquerypedido.repositorio.RepositorioPedido;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
@ActiveProfiles("test")

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class RepositorioPedidoTests {

    @MockBean
    private RepositorioPedido repositorioPedido;

    @Test
    void testBuscarPedidosPorDataCadastro() {
        LocalDate data = LocalDate.now();
        List<Pedido> pedidosMock = List.of(new Pedido(1L, data, 1L, BigDecimal.valueOf(1000), List.of()));
        when(repositorioPedido.findByDataCadastro(data)).thenReturn(pedidosMock);

        List<Pedido> resultado = repositorioPedido.findByDataCadastro(data);

        assertFalse(resultado.isEmpty());
        assertEquals(1, resultado.size());
        assertEquals(data, resultado.get(0).getDataCadastro());
    }

    @Test
    void testBuscarPedidoPorNumeroControle() {
        Long numeroControle = 1L;
        Pedido pedidoMock = new Pedido(numeroControle, LocalDate.now(), 1L, BigDecimal.valueOf(1000), List.of());
        when(repositorioPedido.findByNumeroControle(numeroControle)).thenReturn(java.util.Optional.of(pedidoMock));

        Pedido resultado = repositorioPedido.findByNumeroControle(numeroControle).orElse(null);

        assertNotNull(resultado);
        assertEquals(numeroControle, resultado.getNumeroControle());
    }

    @Test
    void testBuscarPedidosPorNumeroControleEDataCadastro() {
        Long numeroControle = 1L;
        LocalDate data = LocalDate.now();
        List<Pedido> pedidosMock = List.of(new Pedido(numeroControle, data, 1L, BigDecimal.valueOf(1000), List.of()));
        when(repositorioPedido.findByNumeroControleAndDataCadastro(numeroControle, data)).thenReturn(pedidosMock);

        Collection<Pedido> resultado = repositorioPedido.findByNumeroControleAndDataCadastro(numeroControle, data);

        assertFalse(resultado.isEmpty());
        assertEquals(1, resultado.size());
        Pedido primeiroPedido = resultado.iterator().next();
        assertEquals(numeroControle, primeiroPedido.getNumeroControle());
        assertEquals(data, primeiroPedido.getDataCadastro());
    }
}
