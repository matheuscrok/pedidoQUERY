#!/bin/bash

# Nome da rede
NETWORK_NAME="microservice-network-shared"

# Verifica se a rede existe e cria se necessário
if ! docker network ls | grep -q $NETWORK_NAME; then
    echo "Criando rede: $NETWORK_NAME"
    docker network create $NETWORK_NAME
else
    echo "Rede já existe: $NETWORK_NAME"
fi

# Inicia os serviços com Docker Compose
docker-compose up -d
