API de Consulta (Query) - Sistema de Gerenciamento de Pedidos
Descrição
Api desenvolvida para poc e teste com
SpringBoot 3.2.2
Java 21
Kafka
Mysql
Padrão arquitetural CQRS
Foi utilizado records e serviço de mensageria KAFKA


Pré-Requisitos
instale o docker se quiser testar a api com a infraestrutura e os serviços do kafka em conteiners
caso nao queira instalar o kafka local
Caso nao queira instalar docker a api está preparada para teste local 


Configuração
é necessário para teste com docker iniciar a rede interna do docker, o script .sh está preparado para isso

Execução
clone o repositorio e execute o arquivo
-rededocker.sh
para criar a rede e executar o docker-compose
caso não queria testar com kafka a implementacao está adequada para rodar em ambiente local sem a necessidade de conteiners
OBS: as configurações do banco de init script está configurada apenas redirecionar para o banco desejado

# Comandos de exemplo
./rededocker.sh (para criar a rede interna do conteiner docker)

nome da rede interna do docker: microservice-network-shared (IMPORTANTE CRIAR A REDE ANTES DE EXECUTAR OS CONTEINERS)

Consultas Disponíveis
Foi implementado documentação do swagger para acessar segue a url:
http://localhost:8082/swagger-ui/index.html

Segurança e Autenticação
Não foi implementado autenticação

Contato:
COOBSD - Matheus de Castro Sousa 
(62) 9-8160-5134
matheusprogfut@outlook.com