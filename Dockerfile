FROM openjdk:21-jdk-slim AS build

COPY src /app/src
COPY pom.xml /app

WORKDIR /app

RUN apt-get update && apt-get install -y maven && apt-get clean

# test and build
RUN mvn clean package -DskipTests -Dspring.profiles.active=prod


FROM openjdk:21-jdk-slim AS runtime

WORKDIR /app

COPY --from=build /app/target/*.jar /app/query_pedido.jar

EXPOSE 8082
ENV SPRING_PROFILES_ACTIVE=prod

ENTRYPOINT ["java", "-Dspring.profiles.active=prod", "-jar" ,"query_pedido.jar"]
